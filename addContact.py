import time
import xlrd
from form_path import*
from appium import webdriver


class CVContactAutomation(object):

    def __init__(self):
        self.driver = webdriver.Remote(DRIVER_PATH, CAP)
        time.sleep(20)

    def navigate_to_contact_page(self):
        self.driver.find_element_by_xpath(CONTACT_XPATH).click()
        time.sleep(3)
        self.driver.find_element_by_xpath(CONTACT_ADD_BUTTON).click()
        time.sleep(2)
        self.driver.find_element_by_xpath(CONTACT_ADD_FORM).click()
        time.sleep(3)

    def fill_form(self, path, value):
        self.driver.find_element_by_xpath(path).send_keys(value)
        self.driver.back()

    def add_contact(self, first_name, last_name, contact):
        self.navigate_to_contact_page()
        self.fill_form(CONTACT_FIRST_NAME_FIELD, first_name)
        self.fill_form(CONTACT_LAST_NAME_FIELD, last_name)
        self.fill_form(CONTACT_NO_FIELD, contact)
        self.driver.find_element_by_xpath(CONTACT_SAVE_BUTTON).click()
        time.sleep(10)

    def readXcel(self):
        book = ('C:\\Users\\Management\\PycharmProjects\\CloudVoiceExpress\\testdata.xlsx')
        wb = xlrd.open_workbook(book)
        sheet = wb.sheet_by_index(0)

        for i in range(1, sheet.nrows):
            print(i)
            self.add_contact(sheet.cell_value(i, 0), sheet.cell_value(i, 1), sheet.cell_value(i, 2))

if __name__ == "__main__":
    cv_add_contact = CVContactAutomation()
    cv_add_contact.readXcel()
