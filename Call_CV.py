    import time
    from form_path import*
    from appium import webdriver


    class CVCallAutomation(object):

        def __init__(self):
            self.driver = webdriver.Remote(DRIVER_PATH, CAP)
            time.sleep(20)

        def navigate_to_Recents_N_call(self):
            self.driver.find_element_by_xpath(RECENTS_XPATH).click()
            time.sleep(2)
            for x in range(50):
                time.sleep(3)
                self.driver.find_element_by_xpath(FIRST_ENTRY_RECENTS).click()
                time.sleep(20)
                self.driver.find_element_by_accessibility_id(EndCallId).click()
                time.sleep(2)
                print(x, 'Outgoing Calls')

    if __name__ == "__main__":
        cv_recents_call = CVCallAutomation()
        cv_recents_call.navigate_to_Recents_N_call()
